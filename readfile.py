import xml.etree.ElementTree as ET 
import nltk
import re
import json
import codecs
import argparse
args = None
def main():
 global args
 parser = argparse.ArgumentParser(description="Process the xml or raw text in to the POS tagged JSON")
 parser.add_argument('file')
 parser.add_argument('-xml','--xml',help='Used if the input is xml',action="store_true")
 args = parser.parse_args()
 if args.xml: processXML()
 else: processText()
def processText():
 output = []
 file1 = codecs.open(args.file,'r',encoding='utf-8')
 abstract = ''.join(file1.readlines())
 abs_tokenized = nltk.word_tokenize(abstract)
 abs_ner = annotateBrain(abs_tokenized)
 abs_tagged = nltk.pos_tag(abs_tokenized)
 abs_tagged =[abs_tagged[i]+(abs_ner[i][1],) for i in range(len(abs_ner))]
 each = {'pubmed':[], 'abstract':abs_tagged, 'title':[]};
 output.append(each)
 print(json.dumps(output) )
def processXML(): 
 tree = ET.parse(args.file)
 root = tree.getroot();
 output = [];
 for child in root:
    each = {};
    pubmed=child.find('PMID').text;
    title=ET.tostring(child.find('ArticleTitle'),encoding='utf-8',
                      method="xml" );
    abstract= ET.tostring(child.find('AbstractText'),encoding='utf-8', 
                          method="xml" );
    title=title.decode('utf-8');
    abstract=abstract.decode('utf-8');
    #print(title);
    title=re.subn(r"\n", "",title)[0];
    abstract=re.subn(r"\n", "",abstract)[0];
    title = re.sub(r'^<\w*?>(.*?)</\w*?>$',r'\1',title);
    abstract = re.sub(r'^<\w*?>(.*?)</\w*?>$',r'\1',abstract);
    title_tokenized = nltk.word_tokenize(title)
    abs_tokenized = nltk.word_tokenize(abstract)
    title=title.encode('utf-8');
    abstract=abstract.encode('utf-8');
    title_ner = annotateBrain(title_tokenized)
    abs_ner =   annotateBrain(abs_tokenized)
    title_tokenized = [i[0] for i in title_ner];
    abs_tokenized   = [i[0] for i in abs_ner];
    title_tagged    = nltk.pos_tag(title_tokenized)
    abs_tagged      = nltk.pos_tag(abs_tokenized)
    title_tagged =[title_tagged[i]+(title_ner[i][1],) for i in range(len(title_ner))]
    abs_tagged =[abs_tagged[i]+(abs_ner[i][1],) for i in range(len(abs_ner))]
    each = {'pubmed':pubmed, 'abstract':abs_tagged, 'title':title_tagged};
    output.append(each);
    #break;
 print(json.dumps(output) )
def annotateBrain(list):
 #flag=0, O; flag=1, I-BRA
    flag = 0;
    newlist = [];
    i=0;
    while(i<len(list)):
       if(i+2 < len(list)):
         if(list[i]+list[i+1]+list[i+2] == "<BrainRegion>"):
              flag=1;
              newlist.append( (list[i+3],"B-BRA") );
              i=i+4;      
         elif(list[i]+list[i+1]+list[i+2] == "</BrainRegion>"):
              flag=0;
              i=i+3;
         elif(flag==1):
              newlist.append( (list[i],"I-BRA") );
              i=i+1;
         else:
              newlist.append( (list[i],"O") );
              i=i+1;
       else:
              newlist.append( (list[i],"O") );
              i=i+1;
    return newlist; 


if __name__ =="__main__":
    main();

