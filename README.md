# README #

This is the code repository for our CSCI544 final project

### Codes ###

* readfile.py: 
This is the script to process the original XML files or raw text, and tag each token with POS tags.
If it is the pre-annotated brain region training data, the BIO tag (B-BRA, I-BRA, O) will also be added.

* generate_training.py:
This is the script to generate the Megam training data format. 

* brain_ner.pl:
This is the pipeline to tag raw text with brain structures.

* cooccurrence.pl:
This is the script to calculate the co-occurence

* run.js :
This is the Node.js script to parallel run 80 processes at the same time.

*brain_p.py:
This is the script initially used to perform ner training

*brain_c.py
This is used to classify the tags

*megamclassify.py
This is the wrapper built around the megam to make it take every single line as a file and then classify to consider the predicted previous class as the current feature.

*Calculate_all.sh
This is used for the complete performance and evaluation

*Compute_jaccard.py
To compute jaccard Dynamic program way

*ExtractNeuroNames.xslt
XSLT to extract neuro names from XML database

*construct.m
Alternate approach to compute jaccard score using B+ tree kinda approach