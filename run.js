var exec = require("child_process").exec;
var fs = require('fs');
var queue = [];
var filelist = []
var MAX = 80;
var count = 0;
var id = 0;
var run = function(file) {
	exec("perl brain_ner.pl annotated_papers_neuro/"+file+" -xml -id "+
			id+" -dir gene_brain_output_neuro_2 > " +
			"gene_brain_output_neuro_2/" +
			id+".out",function(err,stdout,stderr){
		if(err) console.log(err)
		else console.log(stderr)
		count--;
		process.stderr.write("NOTICE: Finish processing "+file+"\n");
		if(queue.length>0 && count<MAX){
		id++;
		count++;
		var file = queue.shift();
		run(file);
		}
		
	});
	}
filelist = fs.readdirSync("annotated_papers_neuro");
for (var i in filelist) {
	var file = filelist[i];
	if(!file.match(/annotated\.xml$/)) continue;
	file=file.replace(/([\(\)])/g,'\\$1');
	file="'"+file+"'";
	if(count<MAX) {
		count++;
		//console.log(count);
		id++;
		run(file);
	}
	else{
		queue.push(file);
	}
};

