use strict;
use warnings;
use JSON;
use Pod::Usage;
use Getopt::Long;
use File::Basename;
our ($help, $id, $if_xml, $dir);
my $cur_dir = dirname(__FILE__);
GetOptions("help|h"=>\$help, "id=s"=>\$id, "xml"=>\$if_xml,"dir=s"=>\$dir) or pod2usage ();
$help and pod2usage (-verbose=>1, -exitval=>1, -output=>\*STDOUT);
@ARGV>=1 or die "ERROR: You have to enter the file name!\n";
my $file = $ARGV[0];
my $filename = basename($file);
$id||="1"; 
$dir||=".";
if($if_xml)  {  
	   system("perl process_gene_xml.pl $file $dir/$id") and die "ERROR: Running <perl process_gene_xml.pl $file $dir/$id>\n";
	}
else     {  system("cp $file $dir/$id") and die;  }
system("/home/huiyang/usr/Python-2.7.9/python $cur_dir/readfile.py $dir/$id > $dir/${id}tagged.json") and die;
print STDERR "NOTICE: Finish running readfile.py\n";
system("/home/huiyang/usr/Python-2.7.9/python Compute_jaccard.py $dir/${id}tagged.json $dir/${id}data.json") and die;
print STDERR "NOTICE: Finish running Compute_jaccard.py\n";
system("/home/huiyang/usr/Python-2.7.9/python $cur_dir/generate_training.py -id $id -d -out $dir $dir/${id}data.json") and die;
print STDERR "NOTICE: Finish running generate_training.py\n";
my $tags = `/home/huiyang/usr/Python-2.7.9/python $cur_dir/megamclassify.py model_3_3.0 $dir/${id}train_data_3_3.0`;
my @btags = split("\n",$tags);
open(TAGS,"$dir/${id}tagged.json") or die;
system("rm $dir/$id $dir/${id}data.json $dir/${id}dev_data_3_3.0 $dir/${id}tagged.json $dir/${id}train_data_3_3.0");
my $json_text = "";
for (<TAGS>){
	$json_text.=$_;
}
my $perl_scalar = from_json($json_text, {utf8 => 1});
my @orig_tags = @$perl_scalar;
for (my $i=0;$i<@orig_tags;$i++){
	my @abs_list = @{$orig_tags[$i]->{'abstract'}};
	my @brains = ();
	my $brain = "";
	for (my $j=0;$j<@abs_list;$j++){
		$abs_list[$j]->[2]=$btags[$j];
		if($btags[$j] eq "B-BRA"){
			$brain=~s/\s+$//;
			push @brains,$brain if $brain;
			$brain ="";
			$brain.= $abs_list[$j]->[0]." ";
		}
		elsif($btags[$j] eq "I-BRA"){
		    $brain.=$abs_list[$j]->[0]." ";
		}
	}
	$brain=~s/\s+$//;
	push @brains,$brain if $brain;
	#print to_json(\@abs_list);
	print to_json(\@brains)."\n";
	print $filename."\n";
	last;
}








