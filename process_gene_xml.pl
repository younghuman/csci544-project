use strict;
use warnings;
use JSON;
@ARGV==2 or die "ERROR: Two input arguments needed!\n";
my $out_file = $ARGV[1];
my $input_file = $ARGV[0];
open (INPUT, "$input_file") or die;
open (OUT, ">$out_file") or die;
my $string="";
my @genes=();
for (<INPUT>){
	my $line=$_;
	while($line=~s/<GNAT.*?pt="(.*?)".*?>(.*?)<\/GNAT>/####$2####/){
		push @genes, $1;
	}
	$line=~s/<.*?>//g;
	next if ($line =~ /^\W*$/);
    $string .= $line;
}
print OUT $string;
print to_json(\@genes)."\n";