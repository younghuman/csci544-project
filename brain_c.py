#!/usr/bin/env python3
import sys
import pickle
import re
import random
import io

average_vector = pickle.load(open(sys.argv[1], "rb"))
avg = {}
classify = 0
def word_structure(word):
	word_struct = re.sub(r"[A-Z]", "A", word )
	word_struct = re.sub(r"[a-z]", "a", word_struct )
	word_struct = re.sub(r"[0-9]", "1", word_struct )
	word_struct = re.sub(r"^[A-B][0-9][a-z]", ".", word_struct )
	word_struct = re.sub(r"[A-Z]+", "A", word_struct )
	word_struct = re.sub(r"[a-z]+", "a", word_struct )
	word_struct = re.sub(r"[0-9]+", "1", word_struct )
	word_struct = re.sub(r"^[A-B][a-z][0-9]", ".", word_struct )
	return "struct_"+word_struct
for line in sys.stdin:
	words = str.split(line)
	words.insert(0,"BEGIN/BEGIN")
	words.append("END/END")
	previous = "PREVTAG_BEGIN"
	for i in range(1,len(words)-1):
		p = (words[i-1].rsplit("/",1));
		c = words[i].rsplit("/",1);
		n = words[i+1].rsplit("/",1);
		prev_mod = ("prev_"+str(p[0])) 
		curr_mod = ("curr_"+str(c[0]))
		next_mod = ("next_"+str(n[0]))
		curr = c
		prev_tag = ("prevtag_"+str(p[1])) 
		curr_tag = ("currtag_"+str(c[1]))
		next_tag = ("nexttag_"+str(n[1]))
		just_word = str(c[0])
		word_struct = str(c[0]) 
		word_struct = word_structure(word_struct)
		temp = []
		temp.append(curr_mod)
		temp.append(word_struct)
		temp.append(prev_tag)
		temp.append(curr_tag)
		temp.append(previous)
		for category in average_vector:
			for every_word in temp:
				if every_word in average_vector[category]:
					avg[category] = avg.get(category, 0) + average_vector[category][every_word]
		random_index = list(average_vector.keys())
		if len(avg) == 0:
			classify = random.choice(random_index)
		else:
			classify = sorted(avg, key=avg.get, reverse=True)[0]

		previous = "PREVTAG_"+classify
 
		avg = {}
		print (c[0]+"/"+classify,end=" ")
	print ()		
