from __future__ import print_function
import json
import re
import random
from codecs import open
import argparse
import os
def classify_word( word ):
	c_word = re.sub(r"[A-Z]","A",word)
	c_word = re.sub(r"[a-z]","a",c_word)
	c_word = re.sub(r"[0-9]","1",c_word)
	c_word = re.sub(r"[^A-Z0-9a-z]",".",c_word)
	c_word = re.sub(r"[A-Z]+","A",c_word)
	c_word = re.sub(r"[a-z]+","a",c_word)
	c_word = re.sub(r"[0-9]+","1",c_word)
	c_word = re.sub(r"[^A-Za-z0-9]+",".",c_word)
	return c_word
def compileTraining(data, acronym, num=4, jac_min = 0, ner=True):
    length = len(data)
    output = ""
    for i in range(length):
        #The BIO tag as the class
        output += data[i][2]+" " 
        #The current word as a feature    
        output += "c_word:"+data[i][0]+" 1 "
        output += "c_firstword:"+data[i][0][-4:]+" 1 "
	# the structture of word 
	output += "c_struct:" + classify_word(data[i][0])+" 1 "
        #The current POS tag as a feature
        output += "c_tag:" +data[i][1]+" 1 "
        #The current jaccard score as a feature
        jac = data[i][3]
        if jac < jac_min: jac = 0
	#else: jac = 0
        output += "c_jac " + str(jac) +" "
        #The current acronym match as a feature
        if data[i][0] in acronym: 
             output += "acr 1 "
        else:output += "acr 0 " 
        for j in range(i-num,i):
            n = i-j
            if j<0:
               #output += "p"+str(n)+"_word:##start## 1 "
               #output += "p"+str(n)+"_tag:START 1 "
               if ner: output += "p"+str(n)+"_ner:O 0 "
               output += "p"+str(n)+"_jac 0 "
            else:
               output += "p"+str(n)+"_word:"+data[j][0]+" 1 "
               output += "p"+str(n)+"_firstword:"+data[j][0][-4:]+" 1 "
               output += "p"+str(n)+"_struct:" + classify_word(data[j][0])+" 1 "
               output += "p"+str(n)+"_tag:"+data[j][1]+" 1 "
               if ner:
			if data[j][2] == "O":
				output += "p"+str(n)+"_ner:"+data[j][2]+" 0 "
			else:
				output += "p"+str(n)+"_ner:"+data[j][2]+" 1 "
               jac = data[j][3]
               if jac < jac_min: jac = 0
               #else: jac = 0
               output += "p"+str(n)+"_jac "+str(jac)+" "
        for j in range(i+1, i+num+1):
            n = j-i        
            if j>=length:
               #output += "n"+str(n)+"_word:##end## 1 "
               #output += "n"+str(n)+"_tag:END 1 "
               output += "n"+str(n)+"_jac 0 "
            else:       
               output += "n"+str(n)+"_word:"+data[j][0]+" 1 "
               output += "n"+str(n)+"_firstword:"+data[j][0][-4:]+" 1 "
               output += "n"+str(n)+"_struct:" + classify_word(data[j][0])+" 1 "
               output += "n"+str(n)+"_tag:"+data[j][1]+" 1 "
               jac = data[j][3]
               if jac < jac_min: jac = 0
               #else: jac = 0
               output += "n"+str(n)+"_jac "+str(jac)+" "
        output += "\n"
    return output

def compileFeture(file_name):
    fh = open(file_name, "r",encoding="utf-8")
    feature = {}
    for line in fh:
        line = re.sub(r'[\r\n]+', '',line)
        feature[line] = 1
    return feature

if __name__ == "__main__":
    global args
    data_dir = os.path.dirname(os.path.realpath('__file__'))
    parser = argparse.ArgumentParser(description="generate training data")
    parser.add_argument('file')
    parser.add_argument('-d','--dev',dest="dev",help='Whether to generate development data',action="store_true")
    parser.add_argument('-out','--out',dest="outdir",help='The directory to output data',action="store",default=".")
    parser.add_argument('-id','--id',dest="id",help='The id of the current run',action="store",default="")
   
    args = parser.parse_args()
    fh = open(args.file,"r",encoding="utf-8")
    data = json.load(fh)
    #random.shuffle(data)
    length = len(data)
    #print(length)
    if args.dev: sep = int(round(0.9*length))
    num=3
    jac_min=0.3
    fh_train = open(os.path.join(args.outdir,args.id+"train_data_"+str(num)+"_"+str(jac_min*10)), 'w',encoding="utf-8")
    if args.dev: fh_dev   = open(os.path.join(args.outdir,args.id+"dev_data_"+str(num)+"_"+str(jac_min*10)), "w",encoding="utf-8")
    acronym = compileFeture(os.path.join(data_dir,"acronyms"))
    if args.dev: 
      for each in data[0:sep]:
        if 'title' in each:
           out_title = compileTraining(each['title'], acronym,num=num,jac_min=jac_min)
           fh_train.write(out_title)
        if 'abstract' in each:
           out_abs = compileTraining(each['abstract'], acronym,num=num,jac_min=jac_min)
           fh_train.write(out_abs)
      
      for each in data[sep+1:]:
          if 'title' in each:
             out_title = compileTraining(each['title'], acronym,num=num,jac_min=jac_min,ner=False)
             fh_dev.write(out_title)
          if 'abstract' in each:
             out_abs = compileTraining(each['abstract'], acronym,num=num,jac_min=jac_min,ner=False)
             fh_dev.write(out_abs)
    else:
     for each in data:
      if 'title' in each:
         out_title = compileTraining(each['title'], acronym,num=num,jac_min=jac_min,ner=False)
         fh_train.write(out_title)
      if 'abstract' in each:
         out_abs = compileTraining(each['abstract'], acronym,num=num,jac_min=jac_min,ner=False)
         fh_train.write(out_abs)
       
