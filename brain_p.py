#!/usr/bin/env python3
import sys
import pickle
import copy
import random
import re

fin = open(sys.argv[1],"r", errors = 'ignore')

def word_structure(word):
		
		word_struct = re.sub(r"[A-Z]","A",word)
		word_struct = re.sub(r"[a-z]","a",word_struct)
		word_struct = re.sub(r"[0-9]","1",word_struct)
		word_struct = re.sub(r"^[A-B][0-9][a-z]",".",word_struct)
			
		word_struct = re.sub(r"[A-Z]+","A",word_struct)
		word_struct = re.sub(r"[a-z]+","a",word_struct)
		word_struct = re.sub(r"[0-9]+","1",word_struct)
		word_struct = re.sub(r"^[A-B][a-z][0-9]",".",word_struct)

		return "struct_" + word_struct

def learning():
	flag = 0
	err = 0
	average_model = {}
	other_average = {}	
	lines = []
	vector ={}
	average_vector = {}
	num_words = 0
	classes = {}
	vocab = {}
	
	for line in fin:
		words = str.split(line)
		line_to_append = "prev_BOS curr_BOS next_BOS prevtag_BOS currtag_BOS nexttag_BOS struct_A BOS"
		lines.append(line_to_append)
		words.insert(0,"BEGIN/BEGIN/BEGIN")
		words.append("END/END/END")
		for i in range(1,len(words)-1):
			num_words = num_words +  1
			prev_word = (words[i-1].rsplit("/",1));
			curr_word = words[i].rsplit("/",1);
			next_word = words[i+1].rsplit("/",1);
			p = prev_word[0].rsplit("/",1) 
			c = curr_word[0].rsplit("/",1)
			n = next_word[0].rsplit("/",1)
			prev_mod = ("prev_"+str(p[0])) 
			curr_mod = ("curr_"+str(c[0]))
			next_mod = ("next_"+str(n[0]))

			curr = c
			
			prev_tag = ("prevtag_"+str(p[1])) 
			curr_tag = ("currtag_"+str(c[1]))
			next_tag = ("nexttag_"+str(n[1]))
			
			just_word = str(c[0])
			word_struct = str(c[0]) 
				
			word_struct = word_structure(word_struct)
			
			line_to_append = curr_word[1]+" "+prev_mod+" "+curr_mod+" "+word_struct+" " 
			line_to_append = line_to_append+" "+prev_tag+" "+curr_tag
			lines.append(line_to_append)
		
			if curr_word[1] not in classes:
				classes[curr_word[1]] = {}
			vocab[prev_mod] = 0
			vocab[curr_mod] = 0
			vocab[word_struct] = 0	
			vocab[curr_tag] = 0
			vocab[prev_tag] = 0

	vocab["PREVTAG_BEGIN"] = 0
	vocab["PREVTAG_B-BRA"] = 0
	vocab["PREVTAG_I-BRA"] = 0
	vocab["PREVTAG_O"] = 0

	

	for category in classes:
		vector[category] = copy.deepcopy(vocab)
		average_vector[category] = copy.deepcopy(vocab)


	i = 0
	n = num_words
	avg = {}
	for category in average_vector:
		for every_word in vocab:
			curr = vector[category][every_word]  
			average_vector[category][every_word] = average_vector[category][every_word] + ( n * curr )
	for line in lines:
		i = i + 1
		words = str.split(line)
		if words[0] == "prev_BOS":
			previous = "PREVTAG_BEGIN"
			continue
		belongs_to_category = words[0]
		words.pop(0)
		words.append(previous)
			
		for category in vector:
			for every_word in words:
				avg[category] = avg.get(category, 0) + vector[category][every_word]
	
		classify = sorted(avg, key=avg.get, reverse=True)[0]		
		previous = "PREVTAG_"+classify

		if classify != belongs_to_category:
			for every_word in words:
			
				prev = vector[classify][every_word]  
				vector[classify][every_word] = vector[classify].get(every_word) - 1
				curr = vector[classify][every_word]  
				average_vector[classify][every_word] = average_vector[classify][every_word]-((n-i+1)*prev) + ((n-i+1)*curr)

				prev = vector[belongs_to_category][every_word]
				vector[belongs_to_category][every_word] = vector[belongs_to_category].get(every_word) + 1
				curr = vector[belongs_to_category][every_word]
				temp = average_vector[belongs_to_category][every_word] - ((n-i+1) * prev) + ((n-i+1) * curr)
				average_vector[belongs_to_category][every_word] = temp

		avg = {}


	if args.dev_file != None:

		j = 0
		avg = {}
		num_dev = 0
	
		for line in fin1:
			words = str.split(line)
			words.insert(0,"BEGIN/BEGIN/BEGIN")
			words.append("END/END/END")
			previous = "PREVTAG_"+"BEGIN"
			for i in range(1,len(words)-1):
			
				prev_word = (words[i-1].rsplit("/",1));
				curr_word = words[i].rsplit("/",1);
				next_word = words[i+1].rsplit("/",1);
				p = prev_word[0].rsplit("/",1) 
				c = curr_word[0].rsplit("/",1)
				n = next_word[0].rsplit("/",1)

				prev_mod = ("prev_"+str(p[0])) 
				curr_mod = ("curr_"+str(c[0]))					
				next_mod = ("next_"+str(n[0]))

				curr = c
		
				prev_tag = ("prevtag_"+str(p[1])) 
				curr_tag = ("currtag_"+str(c[1]))
				next_tag = ("nexttag_"+str(n[1]))
			
				just_word = str(c[0])
				word_struct = str(c[0]) 

				word_struct = word_structure(word_struct)

				temp = []
				temp.append(prev_mod)
				temp.append(curr_mod)
				temp.append(word_struct)
				temp.append(prev_tag)
				temp.append(curr_tag)
				temp.append(previous)

				belongs_to_category = curr_word[1]

				num_dev = num_dev + 1
			
				for category in average_vector:
					for every_word in temp:
						if every_word in vocab:
							avg[category] = avg.get(category, 0) + average_vector[category][every_word]

				random_index = list(classes.keys())
				if len(avg) == 0:
					classify = random.choice(random_index)
				else: 
					classify = sorted(avg, key=avg.get, reverse=True)[0]		

				previous = "PREVTAG_" + classify 
					
			average_model = copy.deepcopy(average_vector)
	
	pickle.dump(average_model, open(sys.argv[2], "wb"))

def main():
	learning()

if __name__ == "__main__":
	main()
