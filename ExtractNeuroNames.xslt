						<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
						 <xsl:template match="/">

<table>
<tr>
<xsl:for-each select="neuroName/concept/synonyms/synonym">

<xsl:if test="@synonymLanguage='English' or @synonymLanguage='Latin'">

"<xsl:value-of select="."/>",

</xsl:if>

</xsl:for-each>

</tr>
</table>					    
						 </xsl:template>
						
						</xsl:stylesheet>
						