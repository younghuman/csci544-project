import tempfile
import argparse
import sys
import os

previous_tag = ["X","X","X","X"]
previous_count = 0
def format_line( line ):
	global previous_tag
	global previous_count
	i = 0
	while ( i != int(previous_count) ):
		if(previous_tag[i] != "X"):
			if(previous_tag[i] == "O"):
				s = "p"+str(i+1)+"_ner:"+previous_tag[i]+" 0"
	
			else:
				s = "p"+str(i+1)+"_ner:"+previous_tag[i]+" 1"
			line = line + " " + s
		else:
			s = "p"+str(i+1)+"_ner:O"+" 0"
			line = line + " " + s
			
		i = i + 1
	return line

def classify():
	predicted_class = []
	count = 1
	global lines
	import subprocess
	cmd1 = "cat"
	cmd = "megam.opt -predict " + input_arguments.Model_File  + " -nc -fvals multitron /dev/stdin"
	proc = subprocess.Popen(["megam.opt", "-predict", input_arguments.Model_File, "-nc", "-fvals", "multitron", "/dev/stdin"], stdin=subprocess.PIPE,  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	for line in lines:
		temp = format_line(line.strip('\n'))
		#temp.write( format_line(line.strip('\n')) )
		proc.stdin.write(temp+"\n")
		#print temp
		while True:
			out = proc.stdout.readline()
			#if out == b'' and proc.poll() != None:
			#	print ('Ending Call')
			break
		#(out, err) = proc.communicate()
		predicted_class.append(str.split(out)[0])
		previous_tag.pop()
		previous_tag.insert(0, str.split(out)[0])
	for p in predicted_class:
		print p		


if __name__ == '__main__':
	global input_arguments
	global fp
	lines = []
	parser = argparse.ArgumentParser(conflict_handler='resolve')
	parser.add_argument('Model_File', action="store")
	parser.add_argument('Classify_File', action="store")
	input_arguments = parser.parse_args()
	fp = open(input_arguments.Classify_File,"r")
	for line in fp:
		lines.append(line)
	previous_count = int(input_arguments.Model_File[6])
	classify()
