use strict;
use warnings;
use JSON;
opendir(my $dh, "gene_brain_output_neuro_2");
my %gene_brain_hash = ();
for my $file (readdir $dh){
	next if($file!~/\.out$/);
	open(FILE, "gene_brain_output_neuro_2/$file") or die;
	my $i=0;
	my (@genes,@brains,$title);
	my %count = ();
	for my $line (<FILE>){
		if($i==0) {
			$line =~s/[\r\n]+//g;
			@genes = @{from_json($line)};
			$i++;
		}
		elsif($i==1) {
			$line =~s/[\r\n]+//g;
			@brains = @{from_json($line)};
			$i++;
		}
		elsif($i==2) {
			$line =~s/[\r\n]+//g;
			$title = $line;
		}
	}
	next if(not @genes or not @brains or not $title);
	my $sum=0;
	for my $gene (@genes){
		for my $brain (@brains){
			$count{"$gene###$brain"} = 0 if not defined $count{"$gene###$brain"};
			$count{"$gene###$brain"}++;
			$sum++;
		}
	}
	for my $pair (keys %count){
		$gene_brain_hash{$pair} = 0 if not defined $gene_brain_hash{$pair};
		$gene_brain_hash{$pair}+= $count{$pair}/($sum+0.0);
	}	
}
for my $pair (sort {$gene_brain_hash{$b}<=>$gene_brain_hash{$a} } keys %gene_brain_hash){
	print join("\t",($pair, $gene_brain_hash{$pair}))."\n";
}


