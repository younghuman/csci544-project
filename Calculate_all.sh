mkdir data
python generate_training.py
mv train_data_* data
mv dev_data_* data
cd data
for f in $(ls train_data*)
do
	num=$(echo $f | awk -F_ {'print "_"$3"_"$4'})
	echo "=========================================================="
	echo "Running learning for $f"
	megam.opt -nc -maxi 40 -fvals multitron $f > model$num	
	python ../megamclassify.py model$num dev_data$num > out$num
	cat dev_data$num | cut -d" " -f1 > out_prestine$num
	paste out_prestine$num out$num > combine$num
	python3 ../calcscore.py combine$num
	echo "FINISHED learning for $f"
	echo "==========================================================="
done



