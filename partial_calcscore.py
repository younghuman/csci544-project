import sys
try:
	fp = open(sys.argv[1], "r")
except:
	sys.exit(0)
previous_class = "xxx"
classes = { 'BRA':0 }
start = "B-"
correct_classes = dict(classes)
recall_classes = dict(classes)
precision_classes = dict(classes)
previous = 0
for line in fp:
	words = str.split(line)
	if ( "B-" in words[0] ):
		precision_classes[words[0][2:]] = precision_classes[words[0][2:]] + 1
	if ( "B-" in words[1] ):
		recall_classes[words[1][2:]] = recall_classes[words[1][2:]] + 1
	if ( words[0] == words[1] and words[0] != 'O' and words[1] != 'O' ):
		if ( "B-" in words[0] ):
			previous_class = words[0][2:]
			correct_classes[previous_class] = correct_classes[previous_class] + 1
		else:
			previous_class = "xxx"
	else: 
		previous_class = "xxx"

print ( correct_classes )	
print ( recall_classes )	
print ( precision_classes )
correctc = 0
precisionc = 0
recallc = 0
for c in classes:
	precision = correct_classes[c] / precision_classes[c]
	recall = correct_classes[c] / recall_classes[c]
	correctc = correctc + correct_classes[c]
	precisionc = precisionc + precision_classes[c]
	recallc = recallc + recall_classes[c]
	fscore = (2 * precision * recall)/ (precision + recall)
	print ( c, precision, recall, fscore)

#precision = correctc / precisionc
#recall = correctc / recallc
#fscore = (2 * precision * recall)/ (precision + recall)
#print ( "overall", precision, recall, fscore) 		
#print
			
